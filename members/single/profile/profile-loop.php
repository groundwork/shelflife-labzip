<?php do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) : ?>

	<?php while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php do_action( 'bp_before_profile_field_content' ); ?>

			<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">

				<h4><?php //bp_the_profile_group_name(); ?></h4>
				
				<?php if ( $data = bp_get_profile_field_data( 'field=Video' ) ) : ?>
					<div class="champion-video">
            <? if (substr($data, 0, 7) == 'ginger:') { ?>
              <? $ginger = substr($data, 7); ?>
              <iframe src="http://www.ccv.adobe.com/v1/player/<?=$ginger;?>/embed" frameborder="0" allowfullscreen></iframe>
            <? } else if (substr($data, 0, 6) == 'vimeo:') { ?>
              <? $vimeo = substr($data, 6); ?>
              <iframe src="//player.vimeo.com/video/<?=$vimeo?>" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <? } else { ?>
						  <iframe src="http://www.youtube.com/embed/<?php bp_profile_field_data( 'field=Video' );?>?rel=0" frameborder="0" allowfullscreen></iframe>
            <? } ?>
					</div>
				<?php endif ?>
				
				<table class="profile-fields">

					<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

						<?php if ( bp_field_has_data() && bp_get_the_profile_field_name() != "Idea ID" && bp_get_the_profile_field_name() != "Campaign ID") : ?>

							<tr<?php bp_field_css_class(); ?>>

								<td class="label"><?php bp_the_profile_field_name(); ?></td>

								<td class="data"><?php bp_the_profile_field_value(); ?></td>

							</tr>

						<?php endif; ?>

						<?php do_action( 'bp_profile_field_item' ); ?>

					<?php endwhile; ?>

				</table>
				
			</div>

			<?php do_action( 'bp_after_profile_field_content' ); ?>

		<?php endif; ?>

	<?php endwhile; ?>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
