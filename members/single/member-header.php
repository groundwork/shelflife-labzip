<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-default
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>
<?php
/*-----------------------------------------------------------------------------------*/
/* Date: 09/16/12 */
/* Description:  REST webservice call to SalesForce.com to retreive campaign details 
/*-----------------------------------------------------------------------------------*/
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
	//Get the user data and format into the campaign name
	global $bp;
	$user_info = get_userdata( $bp->displayed_user->id ); 
	//$campaign_id = $user_info->first_name . '_' . $user_info->last_name . '_' . date("Y", strtotime($user_info->user_registered));
	$campaign_id = bp_get_profile_field_data( 'field=Campaign ID' );
/*-----------------------------------------------------------------------------------*/
/* End REST webservice call
/*-----------------------------------------------------------------------------------*/
?>

<div id="item-header-social">
		<div class="fb-like" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false"></div>
		<a href="https://twitter.com/share" class="twitter-share-button" data-text="I'm fundraising to change lives with @goworks. Help me reach my goal." data-related="goworks">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div><!-- #item-header-social -->

<div id="item-header-avatar">
	<a href="<?php bp_user_link(); ?>">

		<?php bp_displayed_user_avatar( 'type=full' ); ?> 

	</a>
</div><!-- #item-header-avatar -->

<div id="item-header-content">

	<h2>
		<?php bp_displayed_user_fullname(); ?> 
	</h2>
	
	<?php do_action( 'bp_before_member_header_meta' ); ?>

		<?php if ( $data = bp_get_profile_field_data( 'field=Idea' ) ) : ?>
			<span class="champion-project">Fundraising for: <a href="<?php bp_profile_field_data( 'field=Idea URL' );?>"><?php bp_profile_field_data( 'field=Idea' );?></a></span>
		<?php else : ?>
			<span class="champion-project">Not currently fundraising.</span>
		<?php endif ?>
	
	<div id="item-meta">

		<div id="item-buttons">

			<?php do_action( 'bp_member_header_actions' ); ?>

		</div><!-- #item-buttons -->

		<?php
		/***
		 * If you'd like to show specific profile fields here use:
		 * bp_profile_field_data( 'field=About Me' ); -- Pass the name of the field
		 */
		 do_action( 'bp_profile_header_meta' );

		 ?>

	</div><!-- #item-meta -->

	<?php 				
		//buddypress data pulled locally
		$km_goal = xprofile_get_field_data('Goal');
	?>

	<div class="champion-stats">
		<?
      $expectedRev = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'expectedRev'));
      $fundingToSend = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'fundingToSend'));
    ?>
		<div class="progress-bar">
			<div class="progress-bar-loader" style="width:<?php echo ($fundingToSend / $expectedRev) * 100; ?>%"></div>
		</div>
		<div id="fundraising-goal" class="item-header-box">
			<span class="big">$<?php echo go_salesforce_campaign_field($campaign_id, 'fundingToSend'); ?></span>
			<span class="small">raised of $<?php echo number_format($km_goal); ?></span>	
		</div>
		<div id="backers" class="item-header-box">
			<span class="big"><?php echo go_salesforce_campaign_field($campaign_id, 'numDonationWon'); ?></span>
			<span class="small">donations</span>	
		</div>
		<div id="time-left" class="item-header-box">
			<span class="big"><?php echo go_salesforce_campaign_field($campaign_id, 'timeLeft'); ?></span>
			<span class="small">days left</span>
		</div>
		<div class="donate-champion">
			<?php 
				if (is_plugin_active('go-salesforce/go-salesforce-checkout.php')) {
					$sku = bp_get_profile_field_data( 'field=Campaign ID' );
					echo go_salesforce_display_donate_form($sku);
				}
				else {
			?>
				<form action="https://checkout.google.com/cws/v2/Donations/968278844850954/checkoutForm" id="BB_BuyButtonForm" method="post" name="BB_BuyButtonForm" onsubmit="return validateAmount(this.item_price_1);" target="_top">
				<input name="item_name_1" type="hidden" value="<?php global $bp; $user_info = get_userdata( $bp->displayed_user->id ); echo $user_info->display_name; ?>">
				<input name="item_merchant_id_1" type="hidden" value="<?php echo $campaign_id; ?>"/>
				<input name="item_description_1" type="hidden" value="Thanks for your donation to GO! 100% goes towards development projects.">
				<input name="item_quantity_1" type="hidden" value="1">
				<input name="item_currency_1" type="hidden" value="USD">
				<input name="item_is_modifiable_1" type="hidden" value="true">
				<input name="item_min_price_1" type="hidden" value="0.01">
				<input name="item_max_price_1" type="hidden" value="25000.0">
				<input name="_charset_" type="hidden" value="utf-8">
				<input type="hidden" name="checkout-flow-support.merchant-checkout-flow-support.continue-shopping-url" value="<?php global $bp; echo bp_core_get_user_domain( $bp->displayed_user->id ); ?>">
				<input type="hidden" name="analyticsdata" value="">
				<div class="donate_symbol">
					<span>$</span>
				</div>                       
				<input id="item_price_1" name="item_price_1" onfocus="this.style.color='black'; this.value='';" type="text" value="100">
				<button type="submit" class="button alt">Donate</button>
				</form>
			<?php
				}
			?>
		</div>
	</div><!-- #champion-stats -->
	
</div><!-- #item-header-content -->

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>
