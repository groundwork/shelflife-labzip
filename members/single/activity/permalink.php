<?php get_header() ?>

<!-- #content Starts -->
<?php woo_content_before(); ?>
	<div id="content" class="col-full">

	<div id="main-sidebar-container">    

	<!-- #main Starts -->
<?php woo_main_before(); ?>
	<div id="main">  
         
<!-- BuddyPress Code Starts -->
<div id="bp">

<div class="activity no-ajax" role="main">
	<?php if ( bp_has_activities( 'display_comments=threaded&show_hidden=true&include=' . bp_current_action() ) ) : ?>

		<ul id="activity-stream" class="activity-list item-list">
		<?php while ( bp_activities() ) : bp_the_activity(); ?>

			<?php locate_template( array( 'activity/entry.php' ), true ) ?>

		<?php endwhile; ?>
		</ul>

	<?php endif; ?>
</div>

</div><!-- /#bp -->
<!-- BuddyPress Code Ends -->

	</div><!-- /#main -->
<?php woo_main_after(); ?>
    
	<?php get_sidebar(); ?>

	</div><!-- /#main-sidebar-container -->         

	<?php get_sidebar('alt'); ?>

	</div><!-- /#content -->
<?php woo_content_after(); ?>

<?php get_footer(); ?>
