<?php get_header() ?>

    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
    	<div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <div id="main">  
            
<!-- BUDDYPRESS CODE START -->
<div id="bp">

			<div id="item-header">
				<?php locate_template( array( 'members/single/member-header.php' ), true ) ?>
			</div>

			<div id="item-nav">
				<div class="item-list-tabs no-ajax" id="object-nav">
					<ul>
						<?php bp_get_displayed_user_nav() ?>
					</ul>
				</div>
			</div>


			<div id="item-body">

				<div class="item-list-tabs no-ajax" id="subnav">
					<ul>
						<?php bp_get_options_nav() ?>
					</ul>
				</div>

					<?php if (bp_album_has_pictures() ) : bp_album_the_picture();?>
					
				<div class="picture-single activity">
					<h3><?php bp_album_picture_title() ?></h3>
					
                	<div class="picture-outer-container">
                		<div class="picture-inner-container">
			                <div class="picture-middle">
				                <img src="<?php bp_album_picture_middle_url() ?>" />
				                <?php bp_album_adjacent_links() ?>
			                </div>
		                </div>
	                </div>
	                
					<p class="picture-description"><?php bp_album_picture_desc() ?></p>
	                <p class="picture-meta">
	                <?php bp_album_picture_edit_link()  ?>	
	                <?php bp_album_picture_delete_link()  ?></p>
	                
				<?php bp_album_load_subtemplate( apply_filters( 'bp_album_template_screen_comments', 'album/comments' ) ); ?>
				</div>
					
					<?php else : ?>
					
				<div id="message" class="info">
					<p><?php echo bp_word_or_name( __( "This url is not valid.", 'bp-album' ), __( "Either this url is not valid or picture has restricted access.", 'bp-album' ),false,false ) ?></p>
				</div>
					
					<?php endif; ?>

			</div><!-- #item-body -->


</div><!-- /#bp -->
<!-- BUDDYPRESS CODE END -->

            </div><!-- /#main -->
            <?php woo_main_after(); ?>
    
			<?php locate_template( array( 'sidebar.php' ), true ) ?>
	
		</div><!-- /#main-sidebar-container -->         

		<?php locate_template( array( 'sidebar.alt.php' ), true ) ?>

    </div><!-- /#content -->
	<?php woo_content_after(); ?>

<?php get_footer(); ?>