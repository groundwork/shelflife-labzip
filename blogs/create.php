<?php get_header() ?>

<!-- #content Starts -->
<?php woo_content_before(); ?>
	<div id="content" class="col-full">

	<div id="main-sidebar-container">    

	<!-- #main Starts -->
<?php woo_main_before(); ?>
	<div id="main">  
         
<!-- BuddyPress Code Starts -->
<div id="bp">


		<?php do_action( 'template_notices' ); ?>

			<h3><?php _e( 'Create a Site', 'buddypress' ); ?> &nbsp;<a class="button" href="<?php echo trailingslashit( bp_get_root_domain() . '/' . bp_get_blogs_root_slug() ) ?>"><?php _e( 'Site Directory', 'buddypress' ); ?></a></h3>

		<?php do_action( 'bp_before_create_blog_content' ); ?>

		<?php if ( bp_blog_signup_enabled() ) : ?>

			<?php bp_show_blog_signup_form(); ?>

		<?php else: ?>

			<div id="message" class="info">
				<p><?php _e( 'Site registration is currently disabled', 'buddypress' ); ?></p>
			</div>

		<?php endif; ?>

		<?php do_action( 'bp_after_create_blog_content' ); ?>

</div><!-- /#bp -->
<!-- BuddyPress Code Ends -->

	</div><!-- /#main -->
<?php woo_main_after(); ?>
    
	<?php get_sidebar(); ?>

	</div><!-- /#main-sidebar-container -->         

	<?php get_sidebar('alt'); ?>

	</div><!-- /#content -->
<?php woo_content_after(); ?>

<?php get_footer(); ?>