<?php
/**
 * Single Product Short Description
 */

global $post;
$product = new WC_Product( $post->ID );
$categories = explode(",",strip_tags($product->get_categories()));


if ( ! $post->post_excerpt ) return;
?>
	
<div itemprop="description" class="description">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>

<?php if (!in_array("GO Champion", $categories) and !in_array("Online Store", $categories)) { ?>

<?php if (is_user_logged_in()) : ?>
	<div class="become-champion">
		<form action="<?php echo bp_get_loggedin_user_link(); ?>" method="post">
			<input type="hidden" name="champion-idea" value="<?php global $product; echo the_title() /* . ' in ' . strip_tags($product->get_categories()) */; ?>" />
			<input type="hidden" name="champion-url" value="<?php echo the_permalink(); ?>" />
			<input type="hidden" name="champion-ideaId" value="<?php global $product; echo $product->sku;?>" />
			<button type="submit" class="button alt">Fundraise for This Idea</button>
		</form>
	</div>		
<?php else : ?>
	<div class="become-champion">
		<form action="../../register/" method="post">
			<input type="hidden" name="champion-idea" value="<?php global $product; echo the_title() /* . ' in ' . strip_tags($product->get_categories()) */; ?>" />
			<input type="hidden" name="champion-url" value="<?php echo the_permalink(); ?>" />
			<input type="hidden" name="champion-ideaId" value="<?php global $product; echo $product->sku;?>" />
			<button type="submit" class="button alt">Fundraise for This Idea</button>
		</form>
	</div>				
<?php endif; ?>
<?php }?>
