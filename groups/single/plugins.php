<?php get_header() ?>

<!-- #content Starts -->
<?php woo_content_before(); ?>
	<div id="content" class="col-full">

	<div id="main-sidebar-container">    

	<!-- #main Starts -->
<?php woo_main_before(); ?>
	<div id="main">  
         
<!-- BuddyPress Code Starts -->
<div id="bp">

			<?php if ( bp_has_groups() ) : while ( bp_groups() ) : bp_the_group(); ?>

			<?php do_action( 'bp_before_group_plugin_template' ) ?>

			<div id="item-header">
				<?php locate_template( array( 'groups/single/group-header.php' ), true ) ?>
			</div><!-- #item-header -->

			<div id="item-nav">
				<div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
					<ul>
						<?php bp_get_options_nav() ?>

						<?php do_action( 'bp_group_plugin_options_nav' ) ?>
					</ul>
				</div>
			</div><!-- #item-nav -->

			<div id="item-body">

				<?php do_action( 'bp_before_group_body' ) ?>

				<?php do_action( 'bp_template_content' ) ?>

				<?php do_action( 'bp_after_group_body' ) ?>
			</div><!-- #item-body -->

			<?php do_action( 'bp_after_group_plugin_template' ) ?>

			<?php endwhile; endif; ?>

</div><!-- /#bp -->
<!-- BuddyPress Code Ends -->

	</div><!-- /#main -->
<?php woo_main_after(); ?>
    
	<?php get_sidebar(); ?>

	</div><!-- /#main-sidebar-container -->         

	<?php get_sidebar('alt'); ?>

	</div><!-- /#content -->
<?php woo_content_after(); ?>

<?php get_footer(); ?>