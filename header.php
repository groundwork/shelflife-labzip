<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
 
 global $woo_options;
 global $woocommerce;
 
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />

<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	wp_head();
	woo_head();
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/labstyle.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/custom.css">

<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>

<!--Custom Javascript Functions-->
<script src="/wp-content/themes/shelflife-labzip/custom_functions.js" type="text/javascript"></script>

<!--Google-->
<script src="https://checkout.google.com/files/digital/ga_post.js"></script>
<script> 
	function validateAmount(amount){
		if(amount.value.match( /^[0-9]+(\.([0-9]+))?$/)){
			return true;
		}else{
			alert('You must enter a valid donation.');
			amount.focus();
			return false;
		}
	}
</script>

<!--Image Titles-->
<script>
jQuery(document).ready(function($) {
    $('img[title]').each(function() { $(this).removeAttr('title'); });
});
</script>

<!-- BEGIN Facebook Tracking -->
<script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
    _fbq.push(['addPixelId', '572339306202613']);
    })();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=572339306202613&amp;ev=PixelInitialized" /></noscript>
<!-- END Facebook Tracking -->

<!-- BEGIN Facebook Tracking -->
<?php
if ($_SERVER['REQUEST_URI'] == '/') {
  $fb_tracking_code = '6022846141011'; // Homepage
} else if ($_SERVER['REQUEST_URI'] == '/cart/') {
  $fb_tracking_code = '6022846140011';  // Add to cart
} else if ($_SERVER['REQUEST_URI'] == '/checkout/') {
  $fb_tracking_code = '6022846131011'; // Checkout
} else if(substr($_SERVER['REQUEST_URI'], 0, strlen("/checkout/order-received/")) == "/checkout/order-received/") {
  $fb_tracking_code = '6022846138211'; // Confirmation page
} else {
  $fb_tracking_code = '';
}

if ($fb_tracking_code) { ?>
  <script>(function() {
    var _fbq = window._fbq || (window._fbq = []);
    if (!_fbq.loaded) {
      var fbds = document.createElement('script');
      fbds.async = true;
      fbds.src = '//connect.facebook.net/en_US/fbds.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(fbds, s);
      _fbq.loaded = true;
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
    }
  })();
  window._fbq = window._fbq || [];
  window._fbq.push(['track', '<?= $fb_tracking_code ?>', {'value':'0.01','currency':'USD'}]);
  </script>
  <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=<?= $fb_tracking_code ?>&amp;cd[value]=0.01&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
<? } ?>
<!-- END Facebook Tracking -->

</head>
<body <?php body_class(); ?>>

<? if(strpos($_SERVER['HTTP_HOST'], 'dev.groundworkopportunities.org')) { ?>
	<!-- Dev Environment banner -->
	<script language='javascript'>
		function toggle_dev_menu() {
			dev_menu = document.getElementById('dev_menu');
			if (dev_menu.style.visibility == 'hidden') {
				dev_menu.style.visibility='visible';
			} else {
				dev_menu.style.visibility='hidden';
			}
		}
	</script>
	<div style='position: absolute; top: 0px; left: 0px; z-index: 2001;'><img src='/images/dev_sandbox.png' onClick='toggle_dev_menu();'></img></div>
	<div id='dev_menu' style='visibility: hidden; position: absolute; top: 0px; left: 190px; z-index: 2000; width: 150px; height: 120px; background-color: #0099ff; color: #FFFFFF; border: 1px solid; border-color: #000000; padding: 10px; padding-top: 50px; font-size: 16px;'>
		<div><a href='/wp-admin/' style='color: #FFFFFF;'>WP Admin</a></div>
		<div><a href='/phpmyadmin/' style='color: #FFFFFF;'>MySQL</a></div>
		<div><a href='/error_log/' style='color: #FFFFFF;'>Error Log</a></div>
		<div><a href='https://bitbucket.org/groundwork/go/src' style='color: #FFFFFF;'>Code</a></div>
		<div><a href='http://groundworkopportunities.org' style='color: #FFFFFF;'>Live Site!</a></div>
	</div>
<? } ?>

<!--Analytics-->
<?php include_once('analytics.php') ?>
<!--Facebook-->
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '199972330123502', // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

<?php woo_top(); ?>

	<?php if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) { ?>

	<div id="top">
		<nav class="col-full" role="navigation">
			<?php wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'top-nav', 'menu_class' => 'nav fl', 'theme_location' => 'top-menu' ) ); ?>
		</nav>
	</div><!-- /#top -->

    <?php } ?>

	<header id="header">
	
		<div class="col-full">
		
			<?php
			    $logo = get_template_directory_uri() . '/images/logo.png';
			    if ( isset( $woo_options['woo_logo'] ) && $woo_options['woo_logo'] != '' ) { $logo = $woo_options['woo_logo']; }
			?>
			<?php if ( ! isset( $woo_options['woo_texttitle'] ) || $woo_options['woo_texttitle'] != 'true' ) { ?>
			    <a id="logo" href="<?php bloginfo( 'url' ); ?>" title="<?php bloginfo( 'description' ); ?>">
			    	<img src="<?php echo $logo; ?>" alt="<?php bloginfo( 'name' ); ?>" />
			    </a>
	    	<?php } ?>
	    	
	    	<hgroup>
	    	    
				<h1 class="site-title"><a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			      	
			</hgroup>
			
			<div class="follow-buttons">
				<a class="fbook" href="http://www.facebook.com/goworks" target="_blank">like us on facebook</a>
				<a class="twitter" href="http://twitter.com/goworks" target="_blank">follow us on twitter</a>
			</div>
            <div id="logindiv">
                <a href="/login"><img src="/images/login_button.png" width="90px" height="33px" alt="login"></a>
            </div>			
			<div id="woo_search-2" class="widget widget_woo_search"><div class="search_main fix">
    			<form method="get" class="searchform" action="/">
        		<input type="text" class="field s" name="s" value="Search..." onfocus="if ( this.value == 'Search...' ) { this.value = ''; }" onblur="if ( this.value == '' ) { this.value = 'Search...'; }">
        		<input type="submit" src="/wp-content/themes/shelflife/images/ico-search.png" class="search-submit" alt="submit">
    			</form>    
			</div><!--/.search_main--></div>
			
			<?php if ( isset( $woo_options['woo_ad_top'] ) && $woo_options['woo_ad_top'] == 'true' ) { ?>
        	<div id="topad">
				<?php
					if ( isset( $woo_options['woo_ad_top_adsense'] ) && $woo_options['woo_ad_top_adsense'] != '' ) {
						echo stripslashes( $woo_options['woo_ad_top_adsense'] );
					} else {
						if ( isset( $woo_options['woo_ad_top_url'] ) && isset( $woo_options['woo_ad_top_image'] ) )
				?>
					<a href="<?php echo $woo_options['woo_ad_top_url']; ?>"><img src="<?php echo $woo_options['woo_ad_top_image']; ?>" alt="advert" /></a>
				<?php } ?>
			</div><!-- /#topad -->
        	<?php } ?>
        
        </div><!-- /.col-full -->

	</header><!-- /#header -->
	
<div id="wrapper">

	<nav id="navigation" class="col-full" role="navigation">
	
		<?php
		if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary-menu' ) ) {
			wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'main-nav', 'menu_class' => 'nav fl', 'theme_location' => 'primary-menu' ) );
		} else {
		?>
        <ul id="main-nav" class="nav fl">
			<?php if ( is_page() ) $highlight = 'page_item'; else $highlight = 'page_item current_page_item'; ?>
			<li class="<?php echo $highlight; ?>"><a href="<?php echo home_url( '/' ); ?>"><?php _e( 'Home', 'woothemes' ); ?></a></li>
			<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
		</ul><!-- /#nav -->
        <?php } ?>
        
        <ul class="mini-cart">
		    <li>
		    	<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>" class="cart-parent">
		    		<span> 
		    		<?php 
		    		echo sprintf(_n('<mark>%d item</mark>', '<mark>%d items</mark>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);
		    		echo $woocommerce->cart->get_cart_total();
		    		?>
		    		</span>
		    	</a>
		    	<?php
 		    		
		            echo '<ul class="cart_list">';
		            echo '<li class="cart-title"><h3>'.__('Your Cart Contents', 'woothemes').'</h3></li>';
		               if (sizeof($woocommerce->cart->cart_contents)>0) : foreach ($woocommerce->cart->cart_contents as $cart_item_key => $cart_item) :
		    	           $_product = $cart_item['data'];
		    	           if ($_product->exists() && $cart_item['quantity']>0) :
		    	               echo '<li class="cart_list_product"><a href="'.get_permalink($cart_item['product_id']).'">';
		    	               
		    	               echo $_product->get_image();
		    	               
		    	               echo apply_filters('woocommerce_cart_widget_product_title', $_product->get_title(), $_product).'</a>';
		    	               
		    	               if($_product instanceof woocommerce_product_variation && is_array($cart_item['variation'])) :
		    	                   echo woocommerce_get_formatted_variation( $cart_item['variation'] );
		    	                 endif;
		    	               
		    	               echo '<span class="quantity">' .$cart_item['quantity'].' &times; '.woocommerce_price($_product->get_price()).'</span></li>';
		    	           endif;
		    	       endforeach;
       
		            	else: echo '<li class="empty">'.__('No products in the cart.','woothemes').'</li>'; endif;
		            	if (sizeof($woocommerce->cart->cart_contents)>0) :
		                echo '<li class="total"><strong>';
		
		                if (get_option('js_prices_include_tax')=='yes') :
		                    _e('Total', 'woothemes');
		                else :
		                    _e('Subtotal', 'woothemes');
		                endif;
		    				
		    			
		    				
		                echo ':</strong>'.$woocommerce->cart->get_cart_total();'</li>';
		
		                echo '<li class="buttons"><a href="'.$woocommerce->cart->get_cart_url().'" class="button">'.__('View Cart &rarr;','woothemes').'</a> <a href="'.$woocommerce->cart->get_checkout_url().'" class="button checkout">'.__('Checkout &rarr;','woothemes').'</a></li>';
		            endif;
		            
		            echo '</ul>';
		
		        ?>
		    </li>
	  	</ul>

	</nav><!-- /#navigation -->
	
<?php if (defined('BP_VERSION')): ?>
<?php if(!bp_is_blog_page()):  //deprecates LabSecrets Mods if BP is not installed ?>
<div id="lab" class="labfix">
<?php endif; ?>
<?php endif; ?>
