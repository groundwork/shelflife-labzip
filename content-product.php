<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product, $woocommerce_loop;

$campaign_id = $product->sku;
$categories = explode(",",strip_tags($product->get_categories()));

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibilty
if ( ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;
?>
<li class="product <?php
	if ( $woocommerce_loop['loop'] % $woocommerce_loop['columns'] == 0 )
		echo 'last';
	elseif ( ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] == 0 )
		echo 'first';
	?>">

	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">

		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>

		<h3 class="truncate"><?php the_title(); ?></h3>

		<?php
			/**
			 * woocommerce_after_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_template_loop_price - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item_title' );
		?>

	</a>

	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

<?php if (in_array("GO Project", $categories)) { ?>
<?
  $expectedRev = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'expectedRev'));
  $fundingToSend = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'fundingToSend'));
?>
        <div id="idea-info" class="idea-stats-small">
                <div class="progress-bar">
                        <div class="progress-bar-loader" style="width:<?php echo ($fundingToSend / $expectedRev) * 100; ?>%"></div>
                </div>
                <div id="fundraising-goal" class="item-header-box">
                        <span><b>$<?php echo number_format($expectedRev, 0, '.', ','); ?></b></span>
                        <span class="small">goal</span>
                </div>
                <div id="fundraising-goal" class="item-header-box">
                        <span><b>$<?php echo number_format($fundingToSend, 0, '.', ','); ?></b></span>
                        <span class="small">raised</span>
                </div>
                <div id="time-left" class="item-header-box">
                        <span><b><?php echo go_salesforce_campaign_field($campaign_id, 'timeLeft'); ?></b></span>
                        <span class="small">days</span>
                </div>
        </div>
<?php }?>

</li>
