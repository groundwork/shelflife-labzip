<?php

/*-----------------------------------------------------------------------------------*/
/* LabSecrets Solution Functions - Child theme compatibility with BuddyPress */
/* Based on BPTemplate Pack: http://wordpress.org/extend/plugins/bp-template-pack/ */
/* Version 2.0 */
/* Date: 11/28/11 */
/*-----------------------------------------------------------------------------------*/


// Stop the theme from killing WordPress if BuddyPress is not enabled.
if ( !defined('BP_VERSION') ) {
	
	add_action('wp_head', 'no_bp_plugin_installed');
	return false;
}
function no_bp_plugin_installed() {
	echo do_shortcode('[box type="note"]You need to <a href=\"http://buddypress.org/download/\">install and activate the BuddyPress plugin</a> for this theme to work![/box]');
}

// Load text domain for translation
load_theme_textdomain('buddypress');
load_theme_textdomain('buddypress', get_stylesheet_directory() . '/lang');


// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Check to make sure the active theme is not bp-default
if ( 'bp-default' == get_option( 'template' ) )
	return;

/**
 * Sets up WordPress theme for BuddyPress support.
 *
 * @since 1.2
 */
function lab_theme_setup() {
	global $bp;

	// Load the default BuddyPress AJAX functions if it isn't explicitly disabled
	if ( !(int)get_option( 'bp_tpack_disable_js' ) )
		require_once( BP_PLUGIN_DIR . '/bp-themes/bp-default/_inc/ajax.php' );

if ( defined('BP_VERSION') ) {
	if ( !is_admin() ) {
		// Register buttons for the relevant component templates
		// Friends button
		if ( bp_is_active( 'friends' ) )
			add_action( 'bp_member_header_actions',    'bp_add_friend_button' );

		// Activity button
		if ( bp_is_active( 'activity' ) )
			add_action( 'bp_member_header_actions',    'bp_send_public_message_button' );

		// Messages button
		if ( bp_is_active( 'messages' ) )
			add_action( 'bp_member_header_actions',    'bp_send_private_message_button' );

		// Group buttons
		if ( bp_is_active( 'groups' ) ) {
			add_action( 'bp_group_header_actions',     'bp_group_join_button' );
			add_action( 'bp_group_header_actions',     'bp_group_new_topic_button' );
			add_action( 'bp_directory_groups_actions', 'bp_group_join_button' );
		}

		// Blog button
		if ( bp_is_active( 'blogs' ) )
			add_action( 'bp_directory_blogs_actions',  'bp_blogs_visit_blog_button' );
	        }
	}	
}
add_action( 'after_setup_theme', 'lab_theme_setup', 11 );

/**
 * Enqueues BuddyPress JS and related AJAX functions
 *
 * @since 1.2
 */
function lab_enqueue_scripts() {

	// Add words that we need to use in JS to the end of the page so they can be translated and still used.
	$params = array(
		'my_favs'           => __( 'My Favorites', 'buddypress' ),
		'accepted'          => __( 'Accepted', 'buddypress' ),
		'rejected'          => __( 'Rejected', 'buddypress' ),
		'show_all_comments' => __( 'Show all comments for this thread', 'buddypress' ),
		'show_all'          => __( 'Show all', 'buddypress' ),
		'comments'          => __( 'comments', 'buddypress' ),
		'close'             => __( 'Close', 'buddypress' )
	);

	// BP 1.5+
	if ( version_compare( BP_VERSION, '1.3', '>' ) ) {
		// Bump this when changes are made to bust cache
		$version            = '20110818';
		$params['view']     = __( 'View', 'buddypress' );
	}
	// BP 1.2.x
	else {
		$version = '20110729';

		if ( bp_displayed_user_id() )
			$params['mention_explain'] = sprintf( __( "%s is a unique identifier for %s that you can type into any message on this site. %s will be sent a notification and a link to your message any time you use it.", 'buddypress' ), '@' . bp_get_displayed_user_username(), bp_get_user_firstname( bp_get_displayed_user_fullname() ), bp_get_user_firstname( bp_get_displayed_user_fullname() ) );
	}

	// Enqueue the global JS - Ajax will not work without it
	wp_enqueue_script( 'bp-js', BP_PLUGIN_URL . '/bp-themes/bp-default/_inc/global.js', array( 'jquery' ) );

	// Localize the JS strings
	wp_localize_script( 'bp-js', 'BP_DTheme', $params );
}
add_action( 'wp_enqueue_scripts', 'lab_enqueue_scripts' );


if ( !function_exists( 'lab_use_wplogin' ) ) :
/**
 * During no access requests, redirect users to wp-login for authentication.
 *
 * @since 1.2
 */
function lab_use_wplogin() {
	// returning 2 will automatically use wp-login
	return 2;
}
add_filter( 'bp_no_access_mode', 'lab_use_wplogin' );
endif;

/**
 * Hooks into the 'bp_get_activity_action_pre_meta' action to add secondary activity avatar support
 *
 * @since 1.2
 */
function lab_activity_secondary_avatars( $action, $activity ) {
	// sanity check - some older versions of BP do not utilize secondary activity avatars
	if ( function_exists( 'bp_get_activity_secondary_avatar' ) ) :
		switch ( $activity->component ) {
			case 'groups' :
			case 'friends' :
				// Only insert avatar if one exists
				if ( $secondary_avatar = bp_get_activity_secondary_avatar() ) {
					$reverse_content = strrev( $action );
					$position        = strpos( $reverse_content, 'a<' );
					$action          = substr_replace( $action, $secondary_avatar, -$position - 2, 0 );
				}
				break;
		}
	endif;

	return $action;
}
add_filter( 'bp_get_activity_action_pre_meta', 'lab_activity_secondary_avatars', 10, 2 );


/**  BP 1.2.x *************************************************************/
if ( version_compare( BP_VERSION, '1.3', '<' ) ) :

	/*****
	 * Add support for showing the activity stream as the front page of the site */

	/* Filter the dropdown for selecting the page to show on front to include "Activity Stream" */
	function lab_wp_pages_filter( $page_html ) {
		if ( 'page_on_front' != substr( $page_html, 14, 13 ) )
			return $page_html;

		$selected = false;
		$page_html = str_replace( '</select>', '', $page_html );

		if ( bp_tpack_page_on_front() == 'activity' )
			$selected = ' selected="selected"';

		$page_html .= '<option class="level-0" value="activity"' . $selected . '>' . __( 'Activity Stream', 'buddypress' ) . '</option></select>';
		return $page_html;
	}
	add_filter( 'wp_dropdown_pages', 'lab_wp_pages_filter' );

	/* Hijack the saving of page on front setting to save the activity stream setting */
	function lab_page_on_front_update( $oldvalue, $newvalue ) {
		if ( !is_admin() || !is_super_admin() )
			return false;

		if ( 'activity' == $_POST['page_on_front'] )
			return 'activity';
		else
			return $oldvalue;
	}
	add_action( 'pre_update_option_page_on_front', 'lab_page_on_front_update', 10, 2 );

	/* Load the activity stream template if settings allow */
	function lab_page_on_front_template( $template ) {
		global $wp_query;

		if ( empty( $wp_query->post->ID ) )
			return locate_template( array( 'activity/index.php' ), false );
		else
			return $template;
	}
	add_filter( 'page_template', 'lab_page_on_front_template' );

	/* Return the ID of a page set as the home page. */
	function lab_page_on_front() {
		if ( 'page' != get_option( 'show_on_front' ) )
			return false;

		return apply_filters( 'lab_page_on_front', get_option( 'page_on_front' ) );
	}

	/* Force the page ID as a string to stop the get_posts query from kicking up a fuss. */
	function lab_fix_get_posts_on_activity_front() {
		global $wp_query;

		if ( !empty($wp_query->query_vars['page_id']) && 'activity' == $wp_query->query_vars['page_id'] )
			$wp_query->query_vars['page_id'] = '"activity"';
	}
	add_action( 'pre_get_posts', 'lab_fix_get_posts_on_activity_front' );

endif;


// Stop a non-wootheme from killing BP
function Lab_non_wootheme()
{
if ( !function_exists( 'woo_content_before' ) ): 
	function woo_content_before() {
		}
	function woo_content_after() {
		}				
	function woo_main_before() {
		}					
	function woo_main_after() {
		}	
endif;
}
add_action('wp_head', 'Lab_non_wootheme');
						
						
// Replace WordPress Login Logo With Site Name and Link 
add_filter('login_headerurl', 'my_login_url_local');
function my_login_url_local() {
	return get_bloginfo('url');
}

add_filter('login_headertitle', 'my_login_title_attr');
function my_login_title_attr() {
	return esc_attr(get_bloginfo('name'));
}

add_action('login_head', 'my_style_site_name');
function my_style_site_name() {
?>
	<style type="text/css">
	.login h1 a { width:auto; height:auto; text-indent:0; overflow:visible; text-decoration:none; color:#666; display:block; margin:0; padding:0 10px; background:none; }
	.login h1 a:hover { color:#000; background:none; }
	.login h1 { font-family:'helvetica neue', arial, sans-serif; font-weight:bold; text-align:center; font-size:1.6em; width:310px; position:relative; right:-8px; margin:0 0 1em 0; }
	</style>
<?php
}


// Hide bbPress topics from viewers not logged-in									
function lab_logged_in_bbptopics($have_posts){
    if (!is_user_logged_in()){
        $log_in = '<div class="bbp-template-notice"><p>Sorry, you must be logged-in to view the forums!</p></div>';
        $have_posts = null;

    }
   		echo $log_in;
		return $have_posts;
}
add_filter('bbp_has_topics', 'lab_logged_in_bbptopics');
add_filter('bbp_has_forums', 'lab_logged_in_bbptopics');
add_filter('bbp_has_replies', 'lab_logged_in_bbptopics');


// For use with Slideshow Gallery Pro 												
// http://wordpress.org/extend/plugins/slideshow-gallery-pro/ 						 
function Lab_slider()
{
	if ( function_exists( 'admin_head_gallery_settings' ) ): //checks if wordpress gallery pro is installed 
echo'<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->';
echo'<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>';
echo'<script src="http://lab1128.labclient.com/wp-content/themes/canvas-buddypress/js/example.js"></script>';
endif;
}
add_action('wp_head', 'Lab_slider');


/*-----------------------------------------------------------------------------------*/
/* Miller's Customizations 								 							 */
/*-----------------------------------------------------------------------------------*/

//Change Add to Cart text
add_filter('single_add_to_cart_text', 'woo_custom_cart_button_text');
function woo_custom_cart_button_text() {
	return __('Add to Cart', 'woocommerce');
}

//Disallow image links on blog
update_option('image_default_link_type','none');

//Change sign up link to become-a-champion page
function new_login_link(){
	global $bp;
	if ( is_user_logged_in() )
		return false;
	echo '<li class="bp-login no-arrow"><a href="' . bp_get_root_domain() . '/wp-login.php?redirect_to=' . urlencode( bp_get_root_domain() ) . '">' . __( 'Log In', 'buddypress' ) . '</a></li>';
	// Show "Sign Up" link if user registrations are allowed
	if ( bp_get_signup_allowed() )
		echo '<li class="bp-signup no-arrow"><a href="../become-a-champion/">' . __( 'Sign Up', 'buddypress' ) . '</a></li>';
}
remove_action( 'bp_adminbar_menus', 'bp_adminbar_login_menu', 2 );
add_action( 'bp_adminbar_menus', 'new_login_link', 2 );
    
//Warning if you are already a champion
function champions_existing(){

	if(isset($_POST['champion-idea'])) { 
		$current_user = wp_get_current_user();
		$current_user_id = $current_user->ID;
		if($data = xprofile_get_field_data( 'Idea', $current_user_id)) {
			bp_core_add_message( __('You are already fundraising for ' . $data . '. You cannot fundraise for another cause until you complete this one.', 'buddypress' ), 'error' );
		} else {
			xprofile_set_field_data('Idea', $current_user_id, $_POST['champion-idea']);
			xprofile_set_field_data('Idea URL', $current_user_id, $_POST['champion-url']);
			bp_core_add_message( __('Almost there! Just a few more fields to fill in before your page is active. Already have an account? <a href="../wp-login.php?redirect_to=http%3A%2F%2Fwww.groundworkopportunities.org">Login</a> here.', 'buddypress' ) );
		}
	}	
}
add_action('init', 'champions_existing');


// Set messages from other champions pages										 	
function champions_notifications(){
	if(isset($_POST['champion-message'])) {
		bp_core_add_message( __( stripslashes($_POST['champion-message']), 'buddypress' ) );
		global $woocommerce;	
		$woocommerce->add_message( __( stripslashes($_POST['champion-message']), 'woocommerce') );
	}
}
add_action('init', 'champions_notifications');

// Start sessions for tipping functionality
function start_my_session() {
    if(!session_id()) {
        session_start();
    }
}

function end_my_session() {
    session_destroy ();
}
add_action('init', 'start_my_ession', 1);
add_action('wp_logout', 'end_my_session');
add_action('wp_login', 'end_my_session');


// Tipping functionality															 
function add_product_to_cart() {
	if ( ! is_admin() ) {
		global $woocommerce;
		$product_id = 246922173;
		$found = false;
		//check if product already in cart
		if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
			foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
				$_product = $values['data'];
				if ( $_product->id == $product_id or $_product->product_type == 'subscription')
					$found = true;	
			}
			// if product not found, add it
			if ( !$found && $_SESSION['added'] != 1) {
				$woocommerce->cart->add_to_cart( $product_id);
				$_SESSION['added'] = 1;
			}
		}
	}
}
add_action( 'init', 'add_product_to_cart' );


// Adding BuddyPress Navigation Item - Wall											 
function my_bp_nav_adder() { 
    bp_core_new_nav_item( 
        array( 
            'name' => __('Wall', 'buddypress'), 
            'slug' => 'wall', 
            'position' => 20, 
            'show_for_displayed_user' => true, 
            'screen_function' => 'wall_link', 
            'item_css_id' => 'wall' 
        )
    ); 
    print_r($wp_filter); 
} 

function wall_link () { 
    //add content and call the members plugin.php template 
    add_action( 'bp_template_content', 'my_groups_page_function_to_show_screen_content' ); 
    bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) ); 
}

function my_groups_page_function_to_show_screen_content() { 
	?><div class="fb-comments" data-href="<?php global $bp; echo $bp->displayed_user->domain; ?>" data-num-posts="10" data-width="940"></div><?php 
} 

add_action( 'bp_setup_nav', 'my_bp_nav_adder' ); 


//Adds a link to the WP users list to the users' BuddyPress page
function user_row_actions_bp_view($actions, $user_object) {
	global $bp;
	$actions['view'] = '<a href="' . bp_core_get_user_domain($user_object->ID) . '">' . __('View Champion Page') . '</a>';

	return $actions;
}
add_filter('user_row_actions', 'user_row_actions_bp_view', 10, 2);

//Allow HTML elements in BuddyPress profile fields
function bp_remove_html_tag_filter(){
	remove_filter( 'xprofile_get_field_data', 'wp_filter_kses', 1 ); 
	remove_filter( 'xprofile_set_field_data', 'wp_filter_kses', 1 );
	remove_filter( 'bp_get_the_profile_field_value', 'wp_filter_kses', 1 );
}
add_action('init', 'bp_remove_html_tag_filter'); 
?>