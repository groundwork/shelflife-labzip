*** Shelflife - BuddyPress - bbPress - WooCommerce Changelog ***

1.06.2012 Version 1.5.1
* Fully Compatible with BuddyPress 1.5, bbPress 2.0 and WooCommerce 1.0

1.15.2012 Version 1.5.2
* Corrected Conditional Logic to ensure that BuddyPress plugin is installed AND active

1.18.2012 Version 1.5.2.1
* Adjusted Radio Buttons on Group Creation Page and submit buttons type