<?php
/**
 * Cross-sells
 */
?>

<div class="cross-sells">
	<h2>Support Our Operation Costs</h2>
		<ul class="products">
			<li class="product first">
				<a href="/ideas/operations/">
					<div class="img-wrap"><img width="300" height="150" src="<?= bloginfo('stylesheet_directory').'/images/Support-Our-Operations-450x225.png'?>" class="attachment-shop_catalog wp-post-image" alt="placeholder"></div> <!--/.wrap-->				
				</a>			
			</li>
		</ul>
<div class="clear"></div>
</div>
