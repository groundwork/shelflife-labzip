<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
?>

<?php
/*-----------------------------------------------------------------------------------*/
/* Date: 03/28/13 */
/* Description:  Get the SKU of the product to represent the salesforce campaign ID 
/*-----------------------------------------------------------------------------------*/
	//Get the user data and format into the campaign name
    $product = new WC_Product( $post->ID );
	$campaign_id = $product->sku;
	$categories = explode(",",strip_tags($product->get_categories()));
	$expectedRev = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'expectedRev'));
	$fundingToSend = preg_replace("#[^0-9]#", "", go_salesforce_campaign_field($campaign_id, 'fundingToSend'));
/*-----------------------------------------------------------------------------------*/
/* End get SKU
/*-----------------------------------------------------------------------------------*/
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked woocommerce_show_messages - 10
	 */
	 do_action( 'woocommerce_before_single_product' );
?>

<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_show_product_images hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>

	</div><!-- .summary -->

<?php if (in_array("GO Project", $categories)) { ?>
	<div id="idea-info" class="idea-stats">
		<div class="progress-bar">
			<div class="progress-bar-loader" style="width:<?php echo ($fundingToSend / $expectedRev) * 100; ?>%"></div>
		</div>
		<div id="fundraising-goal" class="item-header-box">
			<span class="big">$<?php echo go_salesforce_campaign_field($campaign_id, 'expectedRev'); ?></span>
			<span class="small">goal</span>
		</div>
		<div id="fundraising-goal" class="item-header-box">
			<span class="big">$<?php echo number_format($fundingToSend, 0, '.', ','); ?></span>
			<span class="small">raised</span>
		</div>
		<div id="time-left" class="item-header-box">
			<span class="big"><?php echo go_salesforce_campaign_field($campaign_id, 'timeLeft'); ?></span>
			<span class="small">days left</span>
		</div>
	</div>
<?php }?>

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
