<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if (!defined('ABSPATH')) exit; ?>

<?php do_action('woocommerce_email_header', $email_heading); ?>
<p style="text-align:center;" align="center"><a href="http://www.facebook.com/goworks"><img src="<?= WP_HOME ?>/images/btn_facebook_email.jpg" alt="facebook page" /></a> <a href="http://www.twitter.com/goworks"><img src="<?= WP_HOME ?>/images/btn_twitter_email.jpg" alt="twitter page" /></a></p>
<p>Dear <?php echo $order->billing_first_name; ?>,</p>
<p>Thank you for leveraging Groundwork Opportunities (GO) for making your $<?php echo $order->get_total();?> donation to our partner cause listed below.  You are wonderful!</p>
<p>Your contribution of $<?php echo $order->get_total(); ?> will help them to propel their work toward sustainable futures for communities around the globe.</p>
<p>When we founded GO we made a pledge to give 100% of personal donations directly to the cause, and your gift is no exception. Our operational expenses are covered by a private community of investors and corporate sponsors so that your donation can go where it is needed most: to the project of your choice.</p>
<p>If you want to see the latest photo, or get the most recent updates about an amazing community of leaders and their ideas to create a world beyond poverty, you can visit GO's blog at <a href='www.goworks.org/blog'>www.goworks.org/blog</a>.  You can also like us on Facebook at <a href="http://www.facebook.com/goworks">www.facebook.com/goworks</a> or follow us on Twitter at <a href="http://www.twitter.com/goworks">www.twitter.com/goworks</a>.</p>
<p>If you want to GO even further, you can start your own fundraising campaign to benefit a project of your choice. Just visit <a href="http://www.goworks.org/become-a-champion/">www.goworks.org/become-a-champion/</a>.</p>
<p>I appreciate your trust in us, and I promise you we'll use your donation well.</p>
<p>Thanks again for your support.</p>
<p>Yours in progress,<br/> Bart</p>

<p>Bartlomiej Jan Skorupa, Executive Director <br /> <a href="http://www.goworks.org">www.goworks.org</a> | <a href="mailto:bart@goworks.org">bart@goworks.org</a> | 415.298.9570</p>

<p> -- <br /> Your contribution is tax-deductible to the full extent allowed by law. Please save or print this receipt for your records. This email certifies that you have made this donation as a charitable contribution and you are not receiving any goods or services in return. Our EIN is 26-1869145. </p>

<?php do_action('woocommerce_email_before_order_table', $order, false); ?>

<h2><?php echo __('Order:', 'woocommerce') . ' ' . $order->get_order_number(); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product', 'woocommerce'); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Quantity', 'woocommerce'); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Price', 'woocommerce'); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( (get_option('woocommerce_downloads_grant_access_after_payment')=='yes' && $order->status=='processing') ? true : false, true, ($order->status=='processing') ? true : false ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action('woocommerce_email_after_order_table', $order, false); ?>

<h2><?php _e('Customer details', 'woocommerce'); ?></h2>

<?php if ($order->billing_email) : ?>
	<p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
<?php endif; ?>
<?php if ($order->billing_phone) : ?>
	<p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
<?php endif; ?>

<?php woocommerce_get_template('emails/email-addresses.php', array( 'order' => $order )); ?>

<?php do_action('woocommerce_email_footer'); ?>
